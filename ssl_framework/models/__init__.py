from ssl_framework.models.abstract_image_model import AbstractImageModel

from ssl_framework.models.jigsaw_image_model import JigsawImageModel
from ssl_framework.models.membank_image_model import MemBankImageModel

MODELS = {'jigsaw_image_model': JigsawImageModel,
          'membank_image_model': MemBankImageModel}