import torch
import torch.nn as nn
import torch.nn.functional as F

from ssl_framework.config.default import cfg
from ssl_framework.models.abstract_image_model import AbstractImageModel
from ssl_framework.misc.utils import Normalize

from ssl_framework.losses.membank import NCEAverage
from ssl_framework.losses.membank import NCECriterion
from ssl_framework.losses.membank import LinearAverage


class MemBankImageModel(AbstractImageModel, nn.Module):
    def __init__(self, num_imgs=0):
        super(MemBankImageModel, self).__init__()

        if not self.eval_mode:
            self.heads = None
            self.fc = nn.Linear(2048, cfg.SSL_TASK.CLASSES)
            self.l2norm = Normalize(2)

            if cfg.NCE.NEGATIVES > 0:
                self.lemniscate = NCEAverage(cfg.SSL_TASK.CLASSES,
                                            num_imgs,
                                            cfg.NCE.NEGATIVES,
                                            cfg.NCE.TEMP,
                                            cfg.NCE.MOMENTUM)
                self.criterion = NCECriterion(num_imgs)
            else:
                self.lemniscate = LinearAverage(cfg.SSL_TASK.CLASSES,
                                                num_imgs,
                                                cfg.NCE.TEMP,
                                                cfg.NCE.MOMENTUM)
                self.criterion = nn.CrossEntropyLoss()

    def forward(self, x, out_feat_keys=None):
        """
        x is a list of image patches
        x: [B, T, C, H, W]
        """
        if self.eval_mode:
            return self.vanilla_forward(x, out_feat_keys)
        else:
            x = self.vanilla_forward(x, None)[-1]

            x = x.view(x.size(0), -1)
            x = self.fc(x)
            x = self.l2norm(x)

            # model output is expected to be a list
            return [x]

    def loss(self, logits, **kwargs):
        """
        input: logits output from the network
        target: target values from ground-truth
        """
        if self.eval_mode:
            assert 'target' in kwargs.keys(),\
                   "pass index argument for this model"

            output = F.log_softmax(logits, dim=1)
            return F.nll_loss(output, kwargs['target']), output
        else:
            assert 'index' in kwargs.keys(),\
                   "pass index argument for this model"

            output = self.lemniscate(logits, kwargs['index'])
            return self.criterion(output, kwargs['index']), output