import numpy as np
import torch
import torchvision.transforms as transforms

from PIL import Image
from torch.utils.data import Dataset

from ssl_framework.config.default import cfg
from ssl_framework.datasets.abstract_dataset import AbstractDataset
from ssl_framework.datasets.abstract_task import AbstractSSLTask


class MemBankImagenet(AbstractDataset, Dataset):
    def __init__(self, split):
        super(MemBankImagenet, self).__init__(split=split)

    def __getitem__(self, index):
        img, target = self.dataset.__getitem__(index)

        if cfg.MODEL.FEATURE_EVAL_MODE:
            return img, target
        else:
            return img, target, index

    def __len__(self):
        return len(self.dataset)

    def generate_transform(self):
        normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                         std=[0.229, 0.224, 0.225])

        if self.eval_mode:
            return transforms.Compose([
                transforms.Resize(256),
                transforms.CenterCrop(224),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                normalize
            ])
        else:
            return transforms.Compose([
                transforms.RandomResizedCrop(224, scale=(0.2,1.)),
                transforms.RandomGrayscale(p=0.2),
                transforms.ColorJitter(0.4, 0.4, 0.4, 0.4),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                normalize
            ])