from ssl_framework.datasets.abstract_task import AbstractSSLTask
from ssl_framework.datasets.abstract_dataset import AbstractDataset

from ssl_framework.datasets.jigsaw_dataset import JigsawDataset
from ssl_framework.datasets.membank_imagenet import MemBankImagenet

TASK_DATASET = {'jigsaw_dataset': JigsawDataset,
                'membank_imagenet': MemBankImagenet}