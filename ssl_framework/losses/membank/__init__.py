from ssl_framework.losses.membank.alias_multinomial import AliasMethod

from ssl_framework.losses.membank.LinearAverage import LinearAverage
from ssl_framework.losses.membank.NCEAverage import NCEAverage
from ssl_framework.losses.membank.NCECriterion import NCECriterion